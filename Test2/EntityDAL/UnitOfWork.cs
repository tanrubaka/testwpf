﻿using System;
using Model;
using System.ComponentModel.Composition;

namespace EntityDAL
{
    [Export(typeof(IUnitOfWork))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class UnitOfWork : IUnitOfWork
    {
        private DataContext _context = new DataContext();
        private EntityRepository<Product> _products;


        public IRepository<Product> Product
        {
            get { return _products ?? (_products = new EntityRepository<Product>(_context)); }
        }

        public bool SaveChanges()
        {
            try
            {
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void Discard()
        {
            _context.Dispose();
            _context = new DataContext();
        }
    }
}