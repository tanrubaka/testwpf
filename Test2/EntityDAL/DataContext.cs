﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Configuration;

namespace EntityDAL
{
    internal class DataContext : DbContext
    {
        public DbSet<Product> Product { get; set; }

        public DataContext()
            : base(AppConfig.ConnectionString)
        { }
    }
}
