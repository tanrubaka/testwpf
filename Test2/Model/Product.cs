﻿using System;

namespace Model
{
    public class Product : IDomainObject
    {
        public int Id { get; private set; }
        public string Name { get; set; }
        public DateTime Registration { get; set; }
        public int Quantity { get; set; }
    }
}