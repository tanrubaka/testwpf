﻿namespace Model
{
    public interface IUnitOfWork
    {
        IRepository<Product> Product { get; }
        bool SaveChanges();
        void Discard();
    }
}