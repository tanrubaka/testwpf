﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using GalaSoft.MvvmLight.Command;
using Model;
using ViewModel.Annotations;
using System.Linq;
using EntityUnitOfWork = EntityDAL.UnitOfWork;

namespace ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        #region Fields

        private ObservableCollection<Product> _products;
        [Import]
        private IUnitOfWork Entity { get; set; }

        #endregion

        public MainViewModel()
        {
            if (Entity == null)
            {
                Entity = new EntityUnitOfWork();
            }
            if (!Entity.Product.GetAll().Any())
            {
                Entity.Product.Add(new Product { Name = "Пицца", Registration = DateTime.UtcNow, Quantity = 2 });
                Entity.Product.Add(new Product { Name = "Торт", Registration = DateTime.UtcNow, Quantity = 3 });
                Entity.SaveChanges();
            }
        }

        #region Properties

        [Import]
        public IViewModelManager ViewModelManager { get; set; }

        public ObservableCollection<Product> Products
        {
            get { return _products ??  (_products = new ObservableCollection<Product>(Entity.Product.GetAll())); }
            set { _products = value; }
        }

        public Product SelectedItem { get; set; }

        #endregion

        void Edit(Product product)
        {
            if (Products.Contains(product))
            {
                var productEdit = Entity.Product.GetAll().SingleOrDefault(p => p.Id == product.Id);
                if (productEdit != null)
                {
                    productEdit = product;
                }
            }
            else
            {
                product.Registration = DateTime.UtcNow;
                Entity.Product.Add(product);
                Products.Add(product);
            }
        }

        void childViewModel_Closed(object sender, ClosedEventArgs e)
        {
            var editProductViewModel = sender as EditProductViewModel;
            if (editProductViewModel != null)
            {
                ViewModelManager.ViewModelClose(editProductViewModel);
                if (e.IsOk)
                {
                    Edit(editProductViewModel.CurrentItem);
                }
            }
        }

        #region Command

        private RelayCommand _addCommand;
        private RelayCommand _deleteCommand;
        private RelayCommand _editCommand;
        private RelayCommand _saveChangesCommand;
        private RelayCommand _localizedCommand;

        public RelayCommand AddCommand
        {
            get { return _addCommand ?? (_addCommand = new RelayCommand(OnAdd)); }
        }

        public RelayCommand DeleteCommand
        {
            get { return _deleteCommand ?? (_deleteCommand = new RelayCommand(OnDelete, CanExecuteDelete)); }
        }

        public RelayCommand EditCommand
        {
            get { return _editCommand ?? (_editCommand = new RelayCommand(OnEdit, CanExecuteEdit)); }
        }

        public RelayCommand SaveChangesCommand
        {
            get { return _saveChangesCommand ?? (_saveChangesCommand = new RelayCommand(OnSaveChanges)); }
        }

        public RelayCommand LocalizedCommand
        {
            get { return _localizedCommand ?? (_localizedCommand = new RelayCommand(OnLocalized)); }
        }

        #endregion

        #region Exec Command

        private void OnAdd()
        {
            var childViewModel = new EditProductViewModel(null);
            childViewModel.Closed += childViewModel_Closed;
            ViewModelManager.ViewModelShow(childViewModel);
        }

        private bool CanExecuteDelete()
        {
            return SelectedItem != null;
        }

        private void OnDelete()
        {
            Entity.Product.Delete(SelectedItem);
            Products.Remove(SelectedItem);
        }

        private bool CanExecuteEdit()
        {
            return SelectedItem != null;
        }

        private void OnEdit()
        {
            var childViewModel = new EditProductViewModel(SelectedItem);
            childViewModel.Closed += childViewModel_Closed;
            ViewModelManager.ViewModelShow(childViewModel);
        }

        private void OnSaveChanges()
        {
            Entity.SaveChanges();
        }

        private void OnLocalized()
        {
            switch (CultureInfo.CurrentCulture.Name)
            {
                case "ru-RU":
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
                    Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("en-US");
                    break;
                case "en-US":
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");
                    Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
                    break;
            }
        }

        #endregion

       
    }
}