﻿using GalaSoft.MvvmLight.Command;
using Model;

namespace ViewModel
{
    public class EditProductViewModel : ViewModelBase
    {
        
        public Product CurrentItem { get; set; }

        private RelayCommand _okCommand;
        private RelayCommand _cancelCommand;

        public EditProductViewModel(Product product)
        {
            CurrentItem = product?? new Product();
        }

        public RelayCommand OkCommand
        {
            get { return _okCommand ?? (_okCommand = new RelayCommand(OnOk, CanExecuteOk)); }
        }

        public RelayCommand CancelCommand
        {
            get { return _cancelCommand ?? (_cancelCommand = new RelayCommand(OnCancel)); }
        }

        private void OnCancel()
        {
            Close(new ClosedEventArgs { IsOk = false });
        }

        private bool CanExecuteOk()
        {
            return !string.IsNullOrEmpty(CurrentItem.Name) && CurrentItem.Quantity > 0;
        }

        

        private void OnOk()
        {
            Close(new ClosedEventArgs{IsOk = true});
        }
    }
}