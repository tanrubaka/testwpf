﻿using System;

namespace ViewModel
{
    public abstract class ViewModelBase
    {
        public delegate void ClosedViewModelHandler(object sender, ClosedEventArgs args);
        public event ClosedViewModelHandler Closed;


        public bool IsClosed { get; set; }

        public void Close(ClosedEventArgs args)
        {
            if (!IsClosed && OnClosing())
            {
                IsClosed = true;
                OnClosed(args);
            }
        }

        protected virtual bool OnClosing()
        {
            return true;
        }

        private void OnClosed(ClosedEventArgs args)
        {
            ClosedViewModelHandler handler = Closed;
            if (handler != null) handler.Invoke(this, args);
        }
    }
}