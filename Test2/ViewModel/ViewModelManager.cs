﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public interface IViewModelManager
    {
        event ViewModelManager.ViewModelShowDelegate ViewModelShowEvent;
        event ViewModelManager.ViewModelCloseDelegate ViewModelCloseEvent;

        void ViewModelShow(ViewModelBase viewModel);
        void ViewModelClose(ViewModelBase viewModel);
    }

    [Export(typeof(IViewModelManager))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class ViewModelManager : IViewModelManager
    {
        public delegate void ViewModelShowDelegate(ViewModelBase viewModel);
        public delegate void ViewModelCloseDelegate(ViewModelBase viewModel);

        public event ViewModelShowDelegate ViewModelShowEvent;
        public event ViewModelCloseDelegate ViewModelCloseEvent;

        public void ViewModelShow(ViewModelBase viewModel)
        {
            OnViewModelShowEvent(viewModel);
        }

        public void ViewModelClose(ViewModelBase viewModel)
        {
            OnViewModelCloseEvent(viewModel);
        }

        private void OnViewModelCloseEvent(ViewModelBase viewModel)
        {
            ViewModelCloseDelegate handler = ViewModelCloseEvent;
            if (handler != null) handler(viewModel);
        }

        private void OnViewModelShowEvent(ViewModelBase viewModel)
        {
            ViewModelShowDelegate handler = ViewModelShowEvent;
            if (handler != null) handler(viewModel);
        }
    }
}
