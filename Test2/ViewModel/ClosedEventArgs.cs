﻿using System;

namespace ViewModel
{
    public class ClosedEventArgs : EventArgs
    {
        public bool IsOk { get; set; }
    }
}