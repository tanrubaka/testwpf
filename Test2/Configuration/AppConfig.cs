﻿using System.Configuration;

namespace Configuration
{
    public static class AppConfig
    {
        public static string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString; }
        }
    }
}
