﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Mef.CommonServiceLocator;
using Microsoft.Practices.ServiceLocation;
using ViewModel;

namespace Test2
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            
            var assembly = new AssemblyCatalog(Assembly.GetEntryAssembly());

            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(assembly);
            catalog.Catalogs.Add(new DirectoryCatalog("."));

            var container = new CompositionContainer(catalog);
            container.ComposeParts(this);

            var locator = new MefServiceLocator(container);
            ServiceLocator.SetLocatorProvider(() => locator);

            this.ViewModelManager.ViewModelShowEvent += ViewManager.ViewShow;
            this.ViewModelManager.ViewModelCloseEvent += ViewManager.ViewClose;

            var mainViewModel = new MainViewModel();

            container.ComposeParts(mainViewModel);


            var mainWindow = new MainWindow { DataContext = mainViewModel };
            mainWindow.Show();
        }

        [Import]
        public IViewModelManager ViewModelManager { get; set; }

        [Import]
        public IViewManager ViewManager { get; set; }
    }
}
