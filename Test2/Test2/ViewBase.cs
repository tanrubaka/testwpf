﻿using System.ComponentModel;
using System.Windows;
using ViewModel;

namespace Test2
{
    public class ViewBase : Window
    {
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            if (!((ViewModelBase)DataContext).IsClosed)
            {

                ((ViewModelBase)DataContext).Close(new ClosedEventArgs());
            }
        }
    }
}