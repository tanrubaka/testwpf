﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using ViewModel;

namespace Test2
{
    public interface IViewManager
    {
        void ViewShow(ViewModelBase viewModel);
        void ViewClose(ViewModelBase viewModel);
    }

    [Export(typeof(IViewManager))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class ViewManager : IViewManager
    {

        public void ViewShow(ViewModelBase viewModel)
        {
            if (!_mapping.ContainsKey(viewModel.GetType()))
            {
                throw new ArgumentException("Error mapping");
            }

            if (_openViewModel.ContainsKey(viewModel.GetType()))
            {
                throw new ArgumentException("View model is open");
            }

            var view = (ViewBase)Activator.CreateInstance(_mapping[viewModel.GetType()]);
            view.DataContext = viewModel;
            view.Show();
            _openViewModel.Add(viewModel.GetType(), view);
        }

        public void ViewClose(ViewModelBase viewModel)
        {
            if (_openViewModel.ContainsKey(viewModel.GetType()))
            {
                var view = _openViewModel[viewModel.GetType()];
                view.Close();
                _openViewModel.Remove(viewModel.GetType());
            }
        }


        private readonly Dictionary<Type, Type> _mapping =
            new Dictionary<Type, Type>
            {
               {typeof(EditProductViewModel), typeof(EditProductView)}
            };

        private Dictionary<Type, ViewBase> _openViewModel = new Dictionary<Type, ViewBase>();



    }
}